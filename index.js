const easyvk   = require('easyvk');
const https    = require('https');
const fs       = require('fs');
const csv      = require('fast-csv');
const log      = require('simple-node-logger').createSimpleLogger(__dirname + '/app.log');

require('dotenv').config({ path: __dirname + '/.env' });
process.env.TZ = 'Europe/Moscow' 
const sentryDutyFile = __dirname + '/temp/sentry_duty.txt';
const todaySentryFile = __dirname + '/temp/today_sentry.json';
const restarters = process.env.RESTARTERS.split(',').map(a => parseInt(a));

const action = typeof(process.argv[2]) != 'undefined' ? process.argv[2] : null;

easyvk({
	access_token: process.env.GROUP_TOKEN
}).then(vk => {
	let today = new Date();
	today.setHours(today.getHours()+3);
	let tomorrow = new Date();
	tomorrow.setHours(tomorrow.getHours()+3);
	tomorrow.setDate(tomorrow.getDate()+1);
	switch (action) {
		case 'tomorrow':
			findSentry(tomorrow).then((result) => {
				if (result)
					fs.writeFile(sentryDutyFile, result.id, function(err){console.log(err)});
				remind(vk, result, false);
			});
			break;
		case 'today':
			findSentry().then((result) => {
				remind(vk, result, true);
			});
			break;
		case 'check':
			findSentry(tomorrow).then((tomorrowResult) => {
				let sentryDuty = fs.readFileSync(sentryDutyFile);
				if (tomorrowResult && tomorrowResult.id == sentryDuty) {
					findSentry().then((todayResult) => {
						vk.call("messages.send", {
							"peer_id": process.env.CHAT_ID,
							"message": `*id${todayResult.id} (${todayResult.name}), сменяющий дежурный *id${tomorrowResult.id} (${tomorrowResult.name}) не ответил на оповещение. Позвоните ему, пожалуйста, и сообщите, что завтра он заступает на дежурство.`
						})
					});
				}
			});
			break;
		/* default action is to start longpolling server */
		default:
			const LPB = vk.bots.longpoll

			LPB.connect({
				forGetLongPollServer: {},
				forLongPollServer: {}
			}).then(({connection}) => {

				//connection.debug(({type, data}) => {
				//	console.log(type, data)
				//})
				

				connection.on('message_new', async(msg) => {
					if (msg.body == 'рестарт' && restarters.includes(msg.user_id)) {
						await vk.call('messages.send', {
							"peer_id": msg.user_id,
							"message": "Перезапускаюсь"
						});
						await fs.writeFile(sentryDutyFile, '', err => {console.log(err)});
						await fs.writeFile(todaySentryFile, '', err => {console.log(err)});
						return process.exit(22);
					}

					let sentryDuty = fs.readFileSync(sentryDutyFile);
					if (sentryDuty == msg.user_id && msg.body.toLocaleLowerCase().replace(/[^а-я]+/g, '').match(/принято/) != null){
						fs.writeFile(sentryDutyFile, '', function(err){console.log(err)});
						vk.call('messages.send', {
							"peer_id": msg.user_id,
							"message": "Спасибо."
						});
						vk.call('messages.send', {
							"peer_id": process.env.CHAT_ID,
							"message": "Дежурный на завтра успешно оповещён."
						});
					}
				})

			}, (error) => {
				console.log(error);
			});
	}
}, (error) => {console.log(error)});

let loadFile = () => {
	return new Promise((resolve, reject) => {
		const filename = __dirname + '/temp/dutylist.csv';
		let file = fs.createWriteStream(filename);
		const request = https.get(process.env.DUTY_LIST, function(response) {
		  	response.pipe(file);
		  	response.on('end', () => {
		  		file.end();
		  		let out = [];
		  		file = fs.createReadStream(filename)
					.pipe(csv.parse({headers: true}))
					.on('data', row => out.push(row))
					.on('end', () => {resolve(out)});
		  	});
		});
	})
}

let findSentry = (date = null) => {
	if (date === null) {
		date = getLocalDate();
	}
	return new Promise((resolve, reject) => {
		let mtime = new Date(0);
		if (fs.existsSync(todaySentryFile)) {
			var stats = fs.statSync(todaySentryFile);
			mtime = new Date(stats.mtime);
			mtime.setHours(mtime.getHours() + 3);
		}
		if (
			convertToDateString(date) == convertToDateString(getLocalDate()) &&
			convertToDateString(mtime) == convertToDateString(date)
		){
			//console.log('use cached today sentry');
			let sentryJSON = fs.readFileSync(todaySentryFile);
			sentry = JSON.parse(sentryJSON);
			resolve(sentry);
		} else {
			loadFile().then((dutyArray) => {
				let readableDate = date.toISOString().split('T')[0].replace(/-/g, '.');
				dutyArray.forEach((v) => {
					if (v.date == readableDate){
						if (convertToDateString(date) == convertToDateString(getLocalDate()))
							saveSentry(v);
						resolve(v);
					}
				});
				resolve(false);
			});
		}

	});
}

let saveSentry = (sentry) => {
	fs.writeFile(todaySentryFile, JSON.stringify(sentry), 'utf8', err => {console.log(err)});
}

let remind = (vk, sentry, today = true) => {
	if (sentry)
		message = today ? `Сегодня дежурный *id${sentry.id} (${sentry.name}).` : `*id${sentry.id} (${sentry.name}), завтра ваше дежурство! Подтвердите, что в курсе, написав боту в личку "принято" (https://vk.com/im?media=&sel=-187931282), иначе придётся просить вам позвонить.`;
	else 
		message = today ? `*id${process.env.DUTY_OFFICER}, на сегодня не назначен дежурный!` : `*id${process.env.DUTY_OFFICER}, на завтра не назначен дежурный!`
	vk.call("messages.send", {
		"peer_id": process.env.CHAT_ID,
		"message": message
	})
}

function convertToDateString(date = new Date()) {
	return date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear();
}

function getLocalDate() {
	let date = new Date();
	date.setHours(date.getHours() + 3);
	return date;
}
